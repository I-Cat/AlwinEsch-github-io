var group__cpp__kodi__network =
[
    [ "WakeOnLan", "group__cpp__kodi__network.html#gace1e815cea9513aaf8133495a3199cf8", null ],
    [ "GetIPAddress", "group__cpp__kodi__network.html#ga348badb393c7613e4cd5bdb849515019", null ],
    [ "GetHostname", "group__cpp__kodi__network.html#ga5d26fe0944835cc2d85d0b0cf490cff5", null ],
    [ "GetUserAgent", "group__cpp__kodi__network.html#ga72673186186832200895a00dd36e6885", null ],
    [ "IsLocalHost", "group__cpp__kodi__network.html#ga744651c550478dc22f418d838be989cc", null ],
    [ "IsHostOnLAN", "group__cpp__kodi__network.html#gaa758052264007c1251ed361f8089d94a", null ],
    [ "URLEncode", "group__cpp__kodi__network.html#gac4320b320b1871e24d37f6bab266f368", null ],
    [ "DNSLookup", "group__cpp__kodi__network.html#gaa4dfb240badbab73880c0d3c02ffa734", null ]
];