var group__cpp__kodi__addon__pvr___defs___p_v_r_capabilities___p_v_r_recording_lifetime_value =
[
    [ "Value Help", "group__cpp__kodi__addon__pvr___defs___p_v_r_capabilities___p_v_r_recording_lifetime_value___help.html", null ],
    [ "PVRRecordingLifetimeValue", "group__cpp__kodi__addon__pvr___defs___p_v_r_capabilities___p_v_r_recording_lifetime_value.html#ga1bafe07f58cfa87f06049704839b4363", null ],
    [ "PVRRecordingLifetimeValue", "group__cpp__kodi__addon__pvr___defs___p_v_r_capabilities___p_v_r_recording_lifetime_value.html#ga75ea860c2faf9569f30cc457ca43113b", null ],
    [ "SetValue", "group__cpp__kodi__addon__pvr___defs___p_v_r_capabilities___p_v_r_recording_lifetime_value.html#ga021ad70d689e7798457030da8385ead4", null ],
    [ "GetValue", "group__cpp__kodi__addon__pvr___defs___p_v_r_capabilities___p_v_r_recording_lifetime_value.html#gae33a3ac7ebb9f5c0ac9d287353c6b7ff", null ],
    [ "SetDescription", "group__cpp__kodi__addon__pvr___defs___p_v_r_capabilities___p_v_r_recording_lifetime_value.html#ga400afd5e9bd0a3fda1f979bbeb19c1b2", null ],
    [ "GetDescription", "group__cpp__kodi__addon__pvr___defs___p_v_r_capabilities___p_v_r_recording_lifetime_value.html#ga10eafac774946c9fda6acd8368e6b254", null ]
];