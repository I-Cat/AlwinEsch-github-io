var group__cpp__kodi__gui___c_window__callbacks =
[
    [ "OnInit", "group__cpp__kodi__gui___c_window__callbacks.html#ga7d98c001ac8d31e48df88346e28bae16", null ],
    [ "OnFocus", "group__cpp__kodi__gui___c_window__callbacks.html#ga7f69d7353e186b8fe4a1560767721501", null ],
    [ "OnClick", "group__cpp__kodi__gui___c_window__callbacks.html#gad741fef5d9a1d2bf07449694e9a9e602", null ],
    [ "OnAction", "group__cpp__kodi__gui___c_window__callbacks.html#ga60811bc39952669bf3397c7dfda928d0", null ],
    [ "GetContextButtons", "group__cpp__kodi__gui___c_window__callbacks.html#ga4d0a4447569d56e8bb8b882004759268", null ],
    [ "OnContextButton", "group__cpp__kodi__gui___c_window__callbacks.html#ga2524910928086c386fb0322db08d6ec8", null ],
    [ "SetIndependentCallbacks", "group__cpp__kodi__gui___c_window__callbacks.html#ga97c29d430604d89e4a1a2a57254fac8c", null ]
];