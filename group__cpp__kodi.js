var group__cpp__kodi =
[
    [ "Definitions, structures and enumerators", "group__cpp__kodi___defs.html", "group__cpp__kodi___defs" ],
    [ "1. Setting control", "group__cpp__kodi__settings.html", "group__cpp__kodi__settings" ],
    [ "Log", "group__cpp__kodi.html#gab3a67917537eaaf1ea958b95e5a3e9d1", null ],
    [ "GetInterface", "group__cpp__kodi.html#ga5e8c4e403f511f0c91104677a75a18d1", null ],
    [ "GetAddonInfo", "group__cpp__kodi.html#ga6a93753e708e44a263a895ae65d30030", null ],
    [ "OpenSettings", "group__cpp__kodi.html#ga981f36ea378461d67c0f0502f678b5b5", null ],
    [ "GetLocalizedString", "group__cpp__kodi.html#ga8296027856e5e592c79e45fc2af6f930", null ],
    [ "UnknownToUTF8", "group__cpp__kodi.html#ga275a5231deebed983e67a97f50c8195e", null ],
    [ "GetLanguage", "group__cpp__kodi.html#ga59fa55ddb5d7a6e4d53b10d7f9a1e0cf", null ],
    [ "QueueFormattedNotification", "group__cpp__kodi.html#ga39c65205ea763e00c402cb0f1f50b289", null ],
    [ "QueueNotification", "group__cpp__kodi.html#gac3374c984f66b0715439aa56e717763d", null ],
    [ "GetMD5", "group__cpp__kodi.html#ga4cd782256ea2690e86421f1466683e24", null ],
    [ "GetTempAddonPath", "group__cpp__kodi.html#ga437ac949d8c4dbe95e9cb04830dd6ff8", null ],
    [ "GetRegion", "group__cpp__kodi.html#ga7b98a686abbbd711c76a1e3892b6c0be", null ],
    [ "GetFreeMem", "group__cpp__kodi.html#ga2dc05ae2b766c3710db48b48b257f951", null ],
    [ "GetGlobalIdleTime", "group__cpp__kodi.html#ga156210ce2221a5a160f51cc044e841a1", null ],
    [ "GetCurrentSkinId", "group__cpp__kodi.html#gaa2b38e3e192a404565552416049c4d30", null ],
    [ "KodiVersion", "group__cpp__kodi.html#ga5417d9134259da3d79f40e9a20a48617", null ],
    [ "GetKeyboardLayout", "group__cpp__kodi.html#ga324988f038efe6fec487718361fb8aa6", null ],
    [ "ChangeKeyboardLayout", "group__cpp__kodi.html#ga6f46744b30f0daf04a1f60f5be35290d", null ]
];