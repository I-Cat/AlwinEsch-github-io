var group__cpp__kodi__addon__game =
[
    [ "Definitions, structures and enumerators", "group__cpp__kodi__addon__game___defs.html", "group__cpp__kodi__addon__game___defs" ],
    [ "1. Basic functions", "group__cpp__kodi__addon__game___base.html", null ],
    [ "2. Game operations", "group__cpp__kodi__addon__game___operation.html", "group__cpp__kodi__addon__game___operation" ],
    [ "3. Hardware rendering operations", "group__cpp__kodi__addon__game___hardware_rendering.html", null ],
    [ "4. Input operations", "group__cpp__kodi__addon__game___input_operations.html", null ],
    [ "5. Serialization operations", "group__cpp__kodi__addon__game___serialization_operations.html", null ],
    [ "6. Cheat operations", "group__cpp__kodi__addon__game___cheat_operations.html", null ]
];