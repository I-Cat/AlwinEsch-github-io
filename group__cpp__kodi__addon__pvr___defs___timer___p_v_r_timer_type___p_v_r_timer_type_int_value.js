var group__cpp__kodi__addon__pvr___defs___timer___p_v_r_timer_type___p_v_r_timer_type_int_value =
[
    [ "Value Help", "group__cpp__kodi__addon__pvr___defs___timer___p_v_r_timer_type___p_v_r_timer_type_int_value___help.html", null ],
    [ "PVRTimerTypeIntValue", "group__cpp__kodi__addon__pvr___defs___timer___p_v_r_timer_type___p_v_r_timer_type_int_value.html#ga352a1396951fc7c136121452d5831e6b", null ],
    [ "PVRTimerTypeIntValue", "group__cpp__kodi__addon__pvr___defs___timer___p_v_r_timer_type___p_v_r_timer_type_int_value.html#ga244b872d985380620cdfa7b6231aa187", null ],
    [ "SetValue", "group__cpp__kodi__addon__pvr___defs___timer___p_v_r_timer_type___p_v_r_timer_type_int_value.html#ga021ad70d689e7798457030da8385ead4", null ],
    [ "GetValue", "group__cpp__kodi__addon__pvr___defs___timer___p_v_r_timer_type___p_v_r_timer_type_int_value.html#gae33a3ac7ebb9f5c0ac9d287353c6b7ff", null ],
    [ "SetDescription", "group__cpp__kodi__addon__pvr___defs___timer___p_v_r_timer_type___p_v_r_timer_type_int_value.html#ga400afd5e9bd0a3fda1f979bbeb19c1b2", null ],
    [ "GetDescription", "group__cpp__kodi__addon__pvr___defs___timer___p_v_r_timer_type___p_v_r_timer_type_int_value.html#ga10eafac774946c9fda6acd8368e6b254", null ]
];