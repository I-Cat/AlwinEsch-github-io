/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Kodi Development", "index.html", [
    [ "General", "general.html", "general" ],
    [ "General Development parts", "general_parts.html", "general_parts" ],
    [ "Revisions", "revisions.html", "revisions" ],
    [ "Skin Development", "skin_parts.html", "skin_parts" ],
    [ "Todo List", "todo.html", null ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Language Development", "modules.html", "modules" ],
    [ "Older versions", "usergroup0.html", [
      [ "Kodi 16.x Jarvis", "^http://mirrors.kodi.tv/docs/python-docs/16.x-jarvis/", null ],
      [ "Kodi 15.x Isengard", "^http://mirrors.kodi.tv/docs/python-docs/15.x-isengard/", null ],
      [ "Kodi 14.x Helix", "^http://mirrors.kodi.tv/docs/python-docs/14.x-helix/", null ],
      [ "XBMC 13.x Gotham", "^http://mirrors.kodi.tv/docs/python-docs/13.0-gotham/", null ],
      [ "XBMC 12.x Frodo", "^http://mirrors.kodi.tv/docs/python-docs/12.2-frodo/", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_addon__rendering_control.html",
"group__cpp__kodi__addon__pvr___base.html#ga4b785a037667529aa782947cbd46d141",
"group__cpp__kodi__addon__pvr___defs___recording___p_v_r___r_e_c_o_r_d_i_n_g___f_l_a_g.html#gae03cdca64371f0641fb8ad562647955a",
"group__cpp__kodi__addon__pvr___defs___timer___p_v_r_timer_type.html",
"group__cpp__kodi__addon__pvr___recordings.html#ga4449397dac24ff7736b693e2d63f5608",
"group__cpp__kodi__gui___c_window.html#gaed1cf9288647970fe4198369cae0fdfc",
"group__cpp__kodi__settings.html#ga0f014aac138ad1d8ac51e5ecfb15e4d5",
"group__python___play_list.html",
"group__python__xbmcgui__listitem.html#ga29777336ef26b461359666cc8ec8b2bd"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';