var group__cpp__kodi__addon__vfs__general =
[
    [ "<strong>Callbacks GetDirectory()</strong>", "group__cpp__kodi__addon__vfs__general__cb___get_directory.html", "group__cpp__kodi__addon__vfs__general__cb___get_directory" ],
    [ "Stat", "group__cpp__kodi__addon__vfs__general.html#gaf49133bcf40e8782db667027b0e2ca8c", null ],
    [ "Exists", "group__cpp__kodi__addon__vfs__general.html#ga1c362e8c2a5fdba8852d475df90e5c1b", null ],
    [ "ClearOutIdle", "group__cpp__kodi__addon__vfs__general.html#ga44a4256d72906e3cf2c3931c32f2043f", null ],
    [ "DisconnectAll", "group__cpp__kodi__addon__vfs__general.html#ga63e4714a25299e480224b20d25b824ea", null ],
    [ "Delete", "group__cpp__kodi__addon__vfs__general.html#gac7f9f211b82b4fb634263d31c6e87521", null ],
    [ "Rename", "group__cpp__kodi__addon__vfs__general.html#ga986e41751d1f27a92b277b6daa099f4f", null ],
    [ "DirectoryExists", "group__cpp__kodi__addon__vfs__general.html#ga9320e6c76fb3b3694da7b7c926849eee", null ],
    [ "RemoveDirectory", "group__cpp__kodi__addon__vfs__general.html#ga76a2fb5655a60cdfbcd21df144b8e0ba", null ],
    [ "CreateDirectory", "group__cpp__kodi__addon__vfs__general.html#ga1a4463ca4ea62ee886adfd92b482ea5a", null ],
    [ "GetDirectory", "group__cpp__kodi__addon__vfs__general.html#gacadc62b194cf04b0d9023cb368e609e7", null ],
    [ "ContainsFiles", "group__cpp__kodi__addon__vfs__general.html#ga6f79b050ce11c2a9e6f904cfa09779f6", null ]
];