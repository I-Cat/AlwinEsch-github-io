var group__cpp__kodi__addon__vfs__filecontrol =
[
    [ "Open", "group__cpp__kodi__addon__vfs__filecontrol.html#ga6e84beed6c3a7f628caaa3146c4c2206", null ],
    [ "OpenForWrite", "group__cpp__kodi__addon__vfs__filecontrol.html#gad2794b102fa6ebf250e0188e143d0ef2", null ],
    [ "Close", "group__cpp__kodi__addon__vfs__filecontrol.html#gac4c4bcfe0c8b6731e0efffcdbd06b52d", null ],
    [ "Read", "group__cpp__kodi__addon__vfs__filecontrol.html#ga9be8e612609a10a43e379850a652d2d7", null ],
    [ "Write", "group__cpp__kodi__addon__vfs__filecontrol.html#ga84c0b70fc7aab687e47888ae819222b3", null ],
    [ "Seek", "group__cpp__kodi__addon__vfs__filecontrol.html#ga479212730734d673f958fd3260f0c75f", null ],
    [ "Truncate", "group__cpp__kodi__addon__vfs__filecontrol.html#gacce61e986c54515af51058c5bd6caf32", null ],
    [ "GetLength", "group__cpp__kodi__addon__vfs__filecontrol.html#gaa0926ec9ef72b1125f50c0e00c32aca6", null ],
    [ "GetPosition", "group__cpp__kodi__addon__vfs__filecontrol.html#gaf5e9036c42f02f6f199d75623db57c69", null ],
    [ "GetChunkSize", "group__cpp__kodi__addon__vfs__filecontrol.html#gac697bfe3fb9317db6225353eeaca9d91", null ],
    [ "IoControl", "group__cpp__kodi__addon__vfs__filecontrol.html#ga541f839f4684ac62cbedeb76a1e96e6c", null ]
];