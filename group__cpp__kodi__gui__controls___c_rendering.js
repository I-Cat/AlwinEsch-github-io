var group__cpp__kodi__gui__controls___c_rendering =
[
    [ "Definitions, structures and enumerators", "group__cpp__kodi__gui__controls___c_rendering___defs.html", null ],
    [ "CRendering", "group__cpp__kodi__gui__controls___c_rendering.html#gae90cb0a646dc72730b8bfacddb94f53b", null ],
    [ "~CRendering", "group__cpp__kodi__gui__controls___c_rendering.html#ga3ba169d99665905fc25a8d5cb0a4629d", null ],
    [ "Create", "group__cpp__kodi__gui__controls___c_rendering.html#ga12847d560dd43205d13b0963d9e395e1", null ],
    [ "Render", "group__cpp__kodi__gui__controls___c_rendering.html#ga0bc3f3606d9cf5304dac90cd7a619194", null ],
    [ "Stop", "group__cpp__kodi__gui__controls___c_rendering.html#gaf6f113a64b92a63894b285b4f2a2c3a5", null ],
    [ "Dirty", "group__cpp__kodi__gui__controls___c_rendering.html#ga7cea65505e98c9989ab92a87b2f2e99b", null ],
    [ "SetIndependentCallbacks", "group__cpp__kodi__gui__controls___c_rendering.html#ga5a42a2e6a29e61e189e3caae47110171", null ]
];