var group__cpp__kodi__vfs___general =
[
    [ "GetFileMD5", "group__cpp__kodi__vfs___general.html#ga39cfce74cc463a14d457385b423f5cb3", null ],
    [ "GetCacheThumbName", "group__cpp__kodi__vfs___general.html#ga8ad60f936581e2a88678b4887c4c2115", null ],
    [ "MakeLegalFileName", "group__cpp__kodi__vfs___general.html#ga857b795585d7268e85975850144adad7", null ],
    [ "MakeLegalPath", "group__cpp__kodi__vfs___general.html#gab053f177a461b97666f395d51d9edd05", null ],
    [ "TranslateSpecialProtocol", "group__cpp__kodi__vfs___general.html#ga02bc64066031bc37d5aeb96cb4ee69d9", null ],
    [ "GetFileName", "group__cpp__kodi__vfs___general.html#ga32a9ca83221508e6289056042cae07d2", null ],
    [ "GetDirectoryName", "group__cpp__kodi__vfs___general.html#ga5dba5415b439919e066fc0fbd9e168e4", null ],
    [ "RemoveSlashAtEnd", "group__cpp__kodi__vfs___general.html#gae425c7a614df9ec6ff1a246bfba924f0", null ],
    [ "GetChunkSize", "group__cpp__kodi__vfs___general.html#ga09e1c44942b50e4cd897774e91efde89", null ],
    [ "IsInternetStream", "group__cpp__kodi__vfs___general.html#ga7e8aa23607583b978afe234a9ce2c101", null ],
    [ "IsOnLAN", "group__cpp__kodi__vfs___general.html#ga174c2e66c6652370d24e09bcb8aac28b", null ],
    [ "IsRemote", "group__cpp__kodi__vfs___general.html#gab38b3f346e91763e7a92c580bb98e00c", null ],
    [ "IsLocal", "group__cpp__kodi__vfs___general.html#ga3d451b5a9c2e2168d97c64fcbd5911e1", null ],
    [ "IsURL", "group__cpp__kodi__vfs___general.html#gadb49f5cc1f6d037acb769838b431e320", null ],
    [ "GetHttpHeader", "group__cpp__kodi__vfs___general.html#gad64759098e0ab8d067c753268fe1279e", null ],
    [ "GetMimeType", "group__cpp__kodi__vfs___general.html#gaf4fae4543b9f2e31da590b44fdc72544", null ],
    [ "GetContentType", "group__cpp__kodi__vfs___general.html#ga426aac793ad35c87f9c7eabe007417e7", null ],
    [ "GetCookies", "group__cpp__kodi__vfs___general.html#ga24e12ad1bfdc152c5a74a7fecb4a7b0a", null ]
];