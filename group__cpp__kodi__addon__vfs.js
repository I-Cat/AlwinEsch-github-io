var group__cpp__kodi__addon__vfs =
[
    [ "Definitions, structures and enumerators", "group__cpp__kodi__addon__vfs___defs.html", "group__cpp__kodi__addon__vfs___defs" ],
    [ "<strong>1. General access functions</strong>", "group__cpp__kodi__addon__vfs__general.html", "group__cpp__kodi__addon__vfs__general" ],
    [ "<strong>2. File editing functions</strong>", "group__cpp__kodi__addon__vfs__filecontrol.html", "group__cpp__kodi__addon__vfs__filecontrol" ],
    [ "CInstanceVFS", "group__cpp__kodi__addon__vfs.html#gaf64445faf2b354ba02aea986d18eb291", null ],
    [ "~CInstanceVFS", "group__cpp__kodi__addon__vfs.html#ga397f2048c35582199ae19d1ece876113", null ]
];