var searchData=
[
  ['hasfeature_3031',['HasFeature',['../classkodi_1_1addon_1_1_c_instance_game.html#a0caf17539e0b322620d9a2762f20e63d',1,'kodi::addon::CInstanceGame']]],
  ['height_3032',['Height',['../group__cpp__kodi__addon__screensaver___c_b.html#ga40afc903300fe3a6804393874e257170',1,'kodi::addon::CInstanceScreensaver::Height()'],['../group__cpp__kodi__addon__visualization___c_b.html#ga40afc903300fe3a6804393874e257170',1,'kodi::addon::CInstanceVisualization::Height()']]],
  ['httpheader_3033',['HttpHeader',['../classkodi_1_1vfs_1_1_http_header.html#a9772cb05c9013b372525f553b98d01a5',1,'kodi::vfs::HttpHeader']]],
  ['hwcontextdestroy_3034',['HwContextDestroy',['../classkodi_1_1addon_1_1_c_instance_game.html#a2ae79c17e174f773ae0a228faa475400',1,'kodi::addon::CInstanceGame']]],
  ['hwcontextreset_3035',['HwContextReset',['../classkodi_1_1addon_1_1_c_instance_game.html#a4e62b5e5f8c681379c267c21d8e68969',1,'kodi::addon::CInstanceGame']]],
  ['hwgetprocaddress_3036',['HwGetProcAddress',['../classkodi_1_1addon_1_1_c_instance_game.html#ae56ab5b89b821a5e84dc163fe424c8f7',1,'kodi::addon::CInstanceGame']]]
];
