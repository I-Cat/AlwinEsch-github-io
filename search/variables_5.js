var searchData=
[
  ['filename_3639',['filename',['../group__cpp__kodi__addon__vfs___defs.html#a7efa5e9c7494c7d4586359300221aa5d',1,'VFSURL']]],
  ['flag_5fsecure_5fdecoder_3640',['FLAG_SECURE_DECODER',['../struct_c_r_y_p_t_o___i_n_f_o.html#a202fc850f5d48b75d9137d98406f7abb',1,'CRYPTO_INFO']]],
  ['folder_3641',['folder',['../struct_v_f_s_dir_entry.html#a92b6fb05a7568b9a345d53f6b600f56d',1,'VFSDirEntry']]],
  ['format_3642',['format',['../structgame__stream__video__properties.html#ae58cee32b99bdf0ef4b56bf7e76a4238',1,'game_stream_video_properties']]],
  ['forward_3643',['forward',['../group__cpp__kodi__addon__vfs___defs.html#afb3a1acc95dba87d46125bbe80299d31',1,'VFS_IOCTRL_CACHE_STATUS_DATA']]],
  ['fps_3644',['fps',['../structgame__system__timing.html#a63da2a617b72a1630998a4d8b4d26cd3',1,'game_system_timing']]]
];
