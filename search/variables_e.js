var searchData=
[
  ['rating_3716',['rating',['../struct_vis_track.html#af3602a84a798e599503b72bb040edeaf',1,'VisTrack']]],
  ['redacted_3717',['redacted',['../group__cpp__kodi__addon__vfs___defs.html#a566d638da144af1b92d87e167410ba48',1,'VFSURL']]],
  ['requested_5fport_3718',['requested_port',['../struct_j_o_y_s_t_i_c_k___i_n_f_o.html#ad36248343b4b585f68bbfd51f36e0899',1,'JOYSTICK_INFO']]],
  ['resource_5fdirectories_3719',['resource_directories',['../struct_addon_props___game.html#a0138ad68adc25e386567f93ae11584d9',1,'AddonProps_Game']]],
  ['resource_5fdirectory_5fcount_3720',['resource_directory_count',['../struct_addon_props___game.html#ad364ba757ea047a99138cc116e4d2686',1,'AddonProps_Game']]],
  ['revision_3721',['revision',['../group__cpp__kodi___defs.html#a5061ba1a268eb999ed21e0c4d7bf609e',1,'kodi_version_t']]],
  ['rotation_3722',['rotation',['../structgame__stream__video__packet.html#aa808196005a0b5247450a34a552c0440',1,'game_stream_video_packet']]]
];
