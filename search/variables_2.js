var searchData=
[
  ['cache_5fcontext_3617',['cache_context',['../structgame__stream__hw__framebuffer__properties.html#a154b5d760b70fc969de1a2301ce90dc4',1,'game_stream_hw_framebuffer_properties']]],
  ['calleraddress_3618',['callerAddress',['../struct_a_d_d_o_n___h_a_n_d_l_e___s_t_r_u_c_t.html#ae075bd3f3792ba4d768f39b30c0b01b3',1,'ADDON_HANDLE_STRUCT']]],
  ['codec_5fid_3619',['codec_id',['../group__cpp__kodi__addon__pvr___defs___stream___p_v_r___c_o_d_e_c.html#aa808d78be870421de837247bca6a9c32',1,'PVR_CODEC']]],
  ['codec_5ftype_3620',['codec_type',['../group__cpp__kodi__addon__pvr___defs___stream___p_v_r___c_o_d_e_c.html#a9e9986eebebb77e309e32abdbe65e185',1,'PVR_CODEC']]],
  ['comment_3621',['comment',['../struct_vis_track.html#ae8c2c4e6dab650ca0dbc32956838ddd9',1,'VisTrack']]],
  ['compile_5fname_3622',['compile_name',['../group__cpp__kodi___defs.html#afe9672e29db448c9cc47fbb6cbb5a8f4',1,'kodi_version_t']]],
  ['context_5ftype_3623',['context_type',['../structgame__stream__hw__framebuffer__properties.html#a9c3a34ffc196049b5535335935b53fde',1,'game_stream_hw_framebuffer_properties']]],
  ['controller_5fid_3624',['controller_id',['../structgame__input__device.html#a950bf7c1ef84921edf82fe7e7fcb756a',1,'game_input_device']]],
  ['currate_3625',['currate',['../group__cpp__kodi__addon__vfs___defs.html#ac205a2546a2e3f22f4ec3e180355faba',1,'VFS_IOCTRL_CACHE_STATUS_DATA']]]
];
