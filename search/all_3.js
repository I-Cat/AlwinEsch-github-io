var searchData=
[
  ['4_2e_20software_20framebuffer_20stream_16',['4. Software framebuffer stream',['../group__cpp__kodi__addon__game___defs___software_framebuffer.html',1,'']]],
  ['4_2e_20input_20operations_17',['4. Input operations',['../group__cpp__kodi__addon__game___input_operations.html',1,'']]],
  ['4_2e_20epg_20tag_18',['4. EPG Tag',['../group__cpp__kodi__addon__pvr___defs__epg.html',1,'']]],
  ['4_2e_20epg_20methods_20_28optional_29_19',['4. EPG methods (optional)',['../group__cpp__kodi__addon__pvr___e_p_g_tag.html',1,'']]],
  ['4_2e_20channel_20edit_20_28optional_29_20',['4. Channel edit (optional)',['../group__cpp__kodi__addon__pvr__supports_channel_edit.html',1,'']]],
  ['4_2e_20class_20cfile_21',['4. class CFile',['../group__cpp__kodi__vfs___c_file.html',1,'']]]
];
