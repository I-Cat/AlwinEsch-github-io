var searchData=
[
  ['accesstime_3608',['accessTime',['../struct_s_t_a_t___s_t_r_u_c_t_u_r_e.html#a62c762dad4517d966e7dda92c962d537',1,'STAT_STRUCTURE']]],
  ['addon_5fpath_3609',['addon_path',['../struct_addon_props___peripheral.html#a74f7156d25d6dcf0731792974f88ebaf',1,'AddonProps_Peripheral']]],
  ['album_3610',['album',['../struct_vis_track.html#abd2bfb2f79c96ed742334da3edb48a79',1,'VisTrack']]],
  ['albumartist_3611',['albumArtist',['../struct_vis_track.html#a1f59686054dd0133f911ae43b417a234',1,'VisTrack']]],
  ['artist_3612',['artist',['../struct_vis_track.html#a4de8da8af30c11f038a75a24cd5f7fae',1,'VisTrack']]],
  ['aspect_5fratio_3613',['aspect_ratio',['../structgame__stream__video__properties.html#a5584bdb427edfe29e0c0c3753b619161',1,'game_stream_video_properties']]],
  ['axis_5fcount_3614',['axis_count',['../struct_j_o_y_s_t_i_c_k___i_n_f_o.html#a6c9f3c39fbcd95dfbdbfd4febefafb63',1,'JOYSTICK_INFO']]]
];
