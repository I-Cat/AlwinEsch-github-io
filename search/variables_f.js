var searchData=
[
  ['sample_5frate_3723',['sample_rate',['../structgame__system__timing.html#a5cd2077a80052e0e93450f6259fa5f92',1,'game_system_timing']]],
  ['selected_3724',['selected',['../struct_s_selection_entry.html#a9ee682957ef18956fbe33afe9b6222fa',1,'SSelectionEntry']]],
  ['sharename_3725',['sharename',['../group__cpp__kodi__addon__vfs___defs.html#a575f0b5f44f0766d36e87d788305da4e',1,'VFSURL']]],
  ['size_3726',['size',['../structgame__stream__audio__packet.html#a854352f53b148adc24983a58a1866d66',1,'game_stream_audio_packet::size()'],['../structgame__stream__video__packet.html#a854352f53b148adc24983a58a1866d66',1,'game_stream_video_packet::size()'],['../struct_s_t_a_t___s_t_r_u_c_t_u_r_e.html#af931a8871310b4dad23f0f0b0f623560',1,'STAT_STRUCTURE::size()'],['../struct_v_f_s_dir_entry.html#af931a8871310b4dad23f0f0b0f623560',1,'VFSDirEntry::size()']]],
  ['statustime_3727',['statusTime',['../struct_s_t_a_t___s_t_r_u_c_t_u_r_e.html#aa9d053a15dedce8092183dbcf02bb7b9',1,'STAT_STRUCTURE']]],
  ['stencil_3728',['stencil',['../structgame__stream__hw__framebuffer__properties.html#af01d4df622c64194e81ba777b3abc4b9',1,'game_stream_hw_framebuffer_properties']]],
  ['supports_5fpoweroff_3729',['supports_poweroff',['../struct_j_o_y_s_t_i_c_k___i_n_f_o.html#aada82c5a7c890d306f6b3d691244e126',1,'JOYSTICK_INFO']]],
  ['supports_5fvfs_3730',['supports_vfs',['../struct_addon_props___game.html#a8e8e2f9080b614d0cdf295b2b85541c1',1,'AddonProps_Game']]]
];
