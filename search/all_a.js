var searchData=
[
  ['bool_210',['bool',['../struct_kodi_to_addon_func_table___video_codec.html#af466ed6e85b22414d1dad93af0beddcf',1,'KodiToAddonFuncTable_VideoCodec::bool(__cdecl *open)(const AddonInstance_VideoCodec *instance'],['../struct_kodi_to_addon_func_table___video_codec.html#ad30ecbc33067ee660cf122c1047b4d42',1,'KodiToAddonFuncTable_VideoCodec::bool(__cdecl *reconfigure)(const AddonInstance_VideoCodec *instance'],['../struct_kodi_to_addon_func_table___video_codec.html#a910e20bdbacbac7a08fbba7489ce9635',1,'KodiToAddonFuncTable_VideoCodec::bool(__cdecl *add_data)(const AddonInstance_VideoCodec *instance']]],
  ['bottom_5fleft_5forigin_211',['bottom_left_origin',['../structgame__stream__hw__framebuffer__properties.html#a80e85b810ae38c95621e67b51e47f9bb',1,'game_stream_hw_framebuffer_properties']]],
  ['browse_212',['browse',['../group__python___dialog.html#ga2ece1ca620087a97233713710eac1b83',1,'XBMCAddon::xbmcgui::Dialog']]],
  ['browsemultiple_213',['browseMultiple',['../group__python___dialog.html#ga987efec260c6ee5a457c96a9c3fcba2f',1,'XBMCAddon::xbmcgui::Dialog']]],
  ['browsesingle_214',['browseSingle',['../group__python___dialog.html#ga5ee90196e7b1d6e8d2f60cbba1f0ccef',1,'XBMCAddon::xbmcgui::Dialog']]],
  ['button_5fcount_215',['button_count',['../struct_j_o_y_s_t_i_c_k___i_n_f_o.html#aa48e30e57e011a82e69acddf5e3760fe',1,'JOYSTICK_INFO']]],
  ['button_20control_216',['Button control',['../skin__button_control.html',1,'skin_controls']]]
];
