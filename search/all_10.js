var searchData=
[
  ['hasfeature_1124',['HasFeature',['../classkodi_1_1addon_1_1_c_instance_game.html#a0caf17539e0b322620d9a2762f20e63d',1,'kodi::addon::CInstanceGame']]],
  ['hat_5fcount_1125',['hat_count',['../struct_j_o_y_s_t_i_c_k___i_n_f_o.html#ab670d38c43e4b0f85833ec5d7a06b3e3',1,'JOYSTICK_INFO']]],
  ['height_1126',['height',['../structgame__stream__video__packet.html#ab2e78c61905b4419fcc7b4cfc500fe85',1,'game_stream_video_packet::height()'],['../group__cpp__kodi__addon__screensaver___c_b.html#ga40afc903300fe3a6804393874e257170',1,'kodi::addon::CInstanceScreensaver::Height()'],['../group__cpp__kodi__addon__visualization___c_b.html#ga40afc903300fe3a6804393874e257170',1,'kodi::addon::CInstanceVisualization::Height()']]],
  ['hostname_1127',['hostname',['../group__cpp__kodi__addon__vfs___defs.html#aad01339e89106fdf68f57ef118956fa9',1,'VFSURL']]],
  ['httpheader_1128',['HttpHeader',['../classkodi_1_1vfs_1_1_http_header.html',1,'HttpHeader'],['../classkodi_1_1vfs_1_1_http_header.html#a9772cb05c9013b372525f553b98d01a5',1,'kodi::vfs::HttpHeader::HttpHeader()']]],
  ['hwcontextdestroy_1129',['HwContextDestroy',['../classkodi_1_1addon_1_1_c_instance_game.html#a2ae79c17e174f773ae0a228faa475400',1,'kodi::addon::CInstanceGame']]],
  ['hwcontextreset_1130',['HwContextReset',['../classkodi_1_1addon_1_1_c_instance_game.html#a4e62b5e5f8c681379c267c21d8e68969',1,'kodi::addon::CInstanceGame']]],
  ['hwgetprocaddress_1131',['HwGetProcAddress',['../classkodi_1_1addon_1_1_c_instance_game.html#ae56ab5b89b821a5e84dc163fe424c8f7',1,'kodi::addon::CInstanceGame']]]
];
