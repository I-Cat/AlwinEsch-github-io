var searchData=
[
  ['game_5ferror_5ffailed_3948',['GAME_ERROR_FAILED',['../group__cpp__kodi__addon__game___defs.html#ggad0fb9f502cf7b3a84f8edb0dc6cbd6efa11cf133fce7983a6aa19d68c351c6a80',1,'Game.h']]],
  ['game_5ferror_5finvalid_5fparameters_3949',['GAME_ERROR_INVALID_PARAMETERS',['../group__cpp__kodi__addon__game___defs.html#ggad0fb9f502cf7b3a84f8edb0dc6cbd6efa317a70cefc1dba6d3d9ae0634b13225e',1,'Game.h']]],
  ['game_5ferror_5fno_5ferror_3950',['GAME_ERROR_NO_ERROR',['../group__cpp__kodi__addon__game___defs.html#ggad0fb9f502cf7b3a84f8edb0dc6cbd6efa51b865ee275ae8e392f3e9fd8594fa90',1,'Game.h']]],
  ['game_5ferror_5fnot_5fimplemented_3951',['GAME_ERROR_NOT_IMPLEMENTED',['../group__cpp__kodi__addon__game___defs.html#ggad0fb9f502cf7b3a84f8edb0dc6cbd6efaf397002d4590537629461eff8b98d014',1,'Game.h']]],
  ['game_5ferror_5fnot_5floaded_3952',['GAME_ERROR_NOT_LOADED',['../group__cpp__kodi__addon__game___defs.html#ggad0fb9f502cf7b3a84f8edb0dc6cbd6efabbc3840b4a211598c8e63e31731d75e8',1,'Game.h']]],
  ['game_5ferror_5frejected_3953',['GAME_ERROR_REJECTED',['../group__cpp__kodi__addon__game___defs.html#ggad0fb9f502cf7b3a84f8edb0dc6cbd6efa2e70ad0417fa9e84872ae765c8f2d991',1,'Game.h']]],
  ['game_5ferror_5frestricted_3954',['GAME_ERROR_RESTRICTED',['../group__cpp__kodi__addon__game___defs.html#ggad0fb9f502cf7b3a84f8edb0dc6cbd6efa68ead95db6f599ff034d99737ff54c92',1,'Game.h']]],
  ['game_5ferror_5funknown_3955',['GAME_ERROR_UNKNOWN',['../group__cpp__kodi__addon__game___defs.html#ggad0fb9f502cf7b3a84f8edb0dc6cbd6efa4e208b896403860c888821ae8477bef7',1,'Game.h']]]
];
