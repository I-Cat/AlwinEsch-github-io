var searchData=
[
  ['data_3626',['data',['../structgame__stream__audio__packet.html#a579f5164b5b16e18ddcdb0296ef81799',1,'game_stream_audio_packet::data()'],['../structgame__stream__video__packet.html#a579f5164b5b16e18ddcdb0296ef81799',1,'game_stream_video_packet::data()']]],
  ['dataaddress_3627',['dataAddress',['../struct_a_d_d_o_n___h_a_n_d_l_e___s_t_r_u_c_t.html#acf790eef44516cce2f19836fb51c66ce',1,'ADDON_HANDLE_STRUCT']]],
  ['dataidentifier_3628',['dataIdentifier',['../struct_a_d_d_o_n___h_a_n_d_l_e___s_t_r_u_c_t.html#a17a1b20cfd0d5894f4452026eb3e4c37',1,'ADDON_HANDLE_STRUCT']]],
  ['date_5ftime_3629',['date_time',['../struct_v_f_s_dir_entry.html#a4f0bcb4705ae487764b2984a83f19aca',1,'VFSDirEntry']]],
  ['debug_5fcontext_3630',['debug_context',['../structgame__stream__hw__framebuffer__properties.html#adc79ea799f6f6ee7f7c300185412aaa0',1,'game_stream_hw_framebuffer_properties']]],
  ['depth_3631',['depth',['../structgame__stream__hw__framebuffer__properties.html#a3c3a47e93d564d08b506d18291d5772a',1,'game_stream_hw_framebuffer_properties']]],
  ['deviceid_3632',['deviceId',['../struct_s_t_a_t___s_t_r_u_c_t_u_r_e.html#a21079ffa80688b89c7dd4ba646a55283',1,'STAT_STRUCTURE']]],
  ['discnumber_3633',['discNumber',['../struct_vis_track.html#aecd322e93357b79d7a67f55c07c2577a',1,'VisTrack']]],
  ['domain_3634',['domain',['../group__cpp__kodi__addon__vfs___defs.html#a52e31162553211bc239fae1a464d7751',1,'VFSURL']]],
  ['driver_5findex_3635',['driver_index',['../struct_p_e_r_i_p_h_e_r_a_l___e_v_e_n_t.html#ad6f546a8e18c12469c2e5082e770a17c',1,'PERIPHERAL_EVENT']]],
  ['duration_3636',['duration',['../struct_vis_track.html#ac6e4b2a3cf932b33832d4e4e4e7cd0de',1,'VisTrack']]]
];
