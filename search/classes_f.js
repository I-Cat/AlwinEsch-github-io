var searchData=
[
  ['vfs_5fcache_5fstatus_5fdata_2461',['VFS_CACHE_STATUS_DATA',['../struct_v_f_s___c_a_c_h_e___s_t_a_t_u_s___d_a_t_a.html',1,'']]],
  ['vfs_5fioctrl_5fcache_5fstatus_5fdata_2462',['VFS_IOCTRL_CACHE_STATUS_DATA',['../group__cpp__kodi__addon__vfs___defs.html#struct_v_f_s___i_o_c_t_r_l___c_a_c_h_e___s_t_a_t_u_s___d_a_t_a',1,'']]],
  ['vfs_5fioctrl_5fnative_5fdata_2463',['VFS_IOCTRL_NATIVE_DATA',['../group__cpp__kodi__addon__vfs___defs.html#struct_v_f_s___i_o_c_t_r_l___n_a_t_i_v_e___d_a_t_a',1,'']]],
  ['vfsdirentry_2464',['VFSDirEntry',['../struct_v_f_s_dir_entry.html',1,'']]],
  ['vfsgetdirectorycallbacks_2465',['VFSGetDirectoryCallbacks',['../struct_v_f_s_get_directory_callbacks.html',1,'']]],
  ['vfsproperty_2466',['VFSProperty',['../struct_v_f_s_property.html',1,'']]],
  ['vfsurl_2467',['VFSURL',['../group__cpp__kodi__addon__vfs___defs.html#struct_v_f_s_u_r_l',1,'']]],
  ['videocodec_5finitdata_2468',['VIDEOCODEC_INITDATA',['../struct_v_i_d_e_o_c_o_d_e_c___i_n_i_t_d_a_t_a.html',1,'']]],
  ['videocodec_5fpicture_2469',['VIDEOCODEC_PICTURE',['../struct_v_i_d_e_o_c_o_d_e_c___p_i_c_t_u_r_e.html',1,'']]],
  ['vis_5finfo_2470',['VIS_INFO',['../struct_v_i_s___i_n_f_o.html',1,'']]],
  ['vistrack_2471',['VisTrack',['../struct_vis_track.html',1,'']]]
];
