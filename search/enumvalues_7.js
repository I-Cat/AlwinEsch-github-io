var searchData=
[
  ['vfs_5fioctrl_5fcache_5fsetrate_4068',['VFS_IOCTRL_CACHE_SETRATE',['../group__cpp__kodi__addon__vfs___defs.html#ggaac68d7bda340dd7a7749f4311353075da9c96dd241e2b868a333291de609bf0f1',1,'VFS.h']]],
  ['vfs_5fioctrl_5fcache_5fstatus_4069',['VFS_IOCTRL_CACHE_STATUS',['../group__cpp__kodi__addon__vfs___defs.html#ggaac68d7bda340dd7a7749f4311353075da16bf6c6589956fcf14ee2f2dfd592b47',1,'VFS.h']]],
  ['vfs_5fioctrl_5finvalid_4070',['VFS_IOCTRL_INVALID',['../group__cpp__kodi__addon__vfs___defs.html#ggaac68d7bda340dd7a7749f4311353075da625387cb90a99d72a1b8d7b0616cb5cb',1,'VFS.h']]],
  ['vfs_5fioctrl_5fnative_4071',['VFS_IOCTRL_NATIVE',['../group__cpp__kodi__addon__vfs___defs.html#ggaac68d7bda340dd7a7749f4311353075da8de16ef84833e1fd186b5ac4d8977a99',1,'VFS.h']]],
  ['vfs_5fioctrl_5fseek_5fpossible_4072',['VFS_IOCTRL_SEEK_POSSIBLE',['../group__cpp__kodi__addon__vfs___defs.html#ggaac68d7bda340dd7a7749f4311353075dac9228f1112ac13033806a3d99437f70a',1,'VFS.h']]],
  ['vfs_5fioctrl_5fset_5fretry_4073',['VFS_IOCTRL_SET_RETRY',['../group__cpp__kodi__addon__vfs___defs.html#ggaac68d7bda340dd7a7749f4311353075dadcc1347c7eb1092e75b48ded8dc4537c',1,'VFS.h']]]
];
