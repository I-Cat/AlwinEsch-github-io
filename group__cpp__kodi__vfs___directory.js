var group__cpp__kodi__vfs___directory =
[
    [ "CreateDirectory", "group__cpp__kodi__vfs___directory.html#ga460ba0ff6f9b5c736ed5267a2b9e1f6d", null ],
    [ "DirectoryExists", "group__cpp__kodi__vfs___directory.html#gac966dda71c24315123d968e542f7f7d5", null ],
    [ "RemoveDirectory", "group__cpp__kodi__vfs___directory.html#ga417b4e3e6216e6446264dc3d8a58839e", null ],
    [ "GetDirectory", "group__cpp__kodi__vfs___directory.html#ga65982b10aaec5abb5ebf6a1c27d28610", null ]
];