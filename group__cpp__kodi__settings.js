var group__cpp__kodi__settings =
[
    [ "CheckSettingString", "group__cpp__kodi__settings.html#ga76aab1b49ad0a022dd64f5102758b2f3", null ],
    [ "GetSettingString", "group__cpp__kodi__settings.html#ga347854e2a98376232a9836dadc27b93f", null ],
    [ "SetSettingString", "group__cpp__kodi__settings.html#ga373f1f68c7665c399a9422e40c9e0925", null ],
    [ "CheckSettingInt", "group__cpp__kodi__settings.html#gadfe67549d9f0ab6e3401113223827457", null ],
    [ "GetSettingInt", "group__cpp__kodi__settings.html#ga0f014aac138ad1d8ac51e5ecfb15e4d5", null ],
    [ "SetSettingInt", "group__cpp__kodi__settings.html#ga1050b631255ee435734ae0d10fa4ac9c", null ],
    [ "CheckSettingBoolean", "group__cpp__kodi__settings.html#gaaffb4c51d85ec3f6f4b7c0f98d990917", null ],
    [ "GetSettingBoolean", "group__cpp__kodi__settings.html#gac9b5bb48021789d00409a173f03eed48", null ],
    [ "SetSettingBoolean", "group__cpp__kodi__settings.html#gac743de36b0b5095fce08100e9b332fc5", null ],
    [ "CheckSettingFloat", "group__cpp__kodi__settings.html#ga81046b870e9ec9351475c28eefd48a76", null ],
    [ "GetSettingFloat", "group__cpp__kodi__settings.html#gad71dc84d559872fa621d05d525317f22", null ],
    [ "SetSettingFloat", "group__cpp__kodi__settings.html#ga9ba4021b4dd99fb951d9d2b5fd3cb0bf", null ],
    [ "CheckSettingEnum", "group__cpp__kodi__settings.html#ga2b444088e9585f5bf6c0ecd076e50412", null ],
    [ "GetSettingEnum", "group__cpp__kodi__settings.html#ga74413fcdd1fa7d510d4f5e116e7e73f3", null ],
    [ "SetSettingEnum", "group__cpp__kodi__settings.html#ga4bbae043d4154992a64c6b10c0051d90", null ]
];