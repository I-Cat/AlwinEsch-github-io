var group__cpp__kodi__addon__audiodecoder =
[
    [ "CInstanceAudioDecoder", "group__cpp__kodi__addon__audiodecoder.html#gac137cf71d81a300a9e3c475188fbbc6d", null ],
    [ "Init", "group__cpp__kodi__addon__audiodecoder.html#gaa814c1d8cc5758f05336554155ec1faf", null ],
    [ "ReadPCM", "group__cpp__kodi__addon__audiodecoder.html#ga56033a1416a00686fd677118c2ad29b4", null ],
    [ "Seek", "group__cpp__kodi__addon__audiodecoder.html#gab978901f9152eefcc5b6fe807fc55574", null ],
    [ "ReadTag", "group__cpp__kodi__addon__audiodecoder.html#ga57b721a53617b9f024896c7a82a6b150", null ],
    [ "TrackCount", "group__cpp__kodi__addon__audiodecoder.html#gad339b85b3566aee42195d3b101ca9afa", null ]
];